<?php
function sesio(){
  $link = openDb();
  $iniciasecio = '
    <li class="nav-item mx-0 mx-lg-1" >
      <form action="" method="post">
        <div class="row">
          <div class="col-md-4"><input class="form-control" name="name" type="text" placeholder="User" Style="white-space:nowrap;"></div>
          <div class="col-md-4"><input class="form-control" name="pass" type="password"  placeholder="Password" Style="white-space:nowrap;"></div>
          <div class="col-md-3"><input class="btn btn-primary" type="submit" value="Log in" Style="white-space:nowrap; width: 100%;"></div>
        </div>
      </form>
    </li>';

  if(isset($_SESSION['user'])){
    if (isset($_POST['logout'])){
      session_destroy();
      return ($iniciasecio);
    }else {
      return '
        <li class="nav-item mx-0 mx-lg-1">
          <h4 class="text-uppercase mb-4" Style="color: white; height: 90%; display: flex; align-items: center;">'.base64_decode($_SESSION['user']).'</h4>
        </li>
        <li class="nav-item mx-0 mx-lg-1">
          <form action="" method="post" Style="height: 90%; display: flex; align-items: center;">
            <input type="hidden" name="logout" type="text" >
            <input class="btn btn-primary" type="submit" value="Disconnect">
          </form>
        </li>';
    }

  } else{
    if(isset($_POST['name']) && isset($_POST["pass"])){
      $conservar = '0-9a-zA-Z'; // Caracteres a conserva
      $regex = sprintf('~[^%s]++~i', $conservar); 
      $name= preg_replace($regex, ' ', $_POST["name"]);
      $pass= preg_replace($regex, ' ', $_POST["pass"]);

      $name = base64_encode($name);
      $pass = base64_encode($pass);

      $query = executa_query("SELECT name FROM users WHERE passw = '".$pass."' and name = '".$name."';", $link);

      while($row = mysqli_fetch_array($query)) { 
        $confirmation = $row[0];
      }
      if (isset($confirmation)) {
        if ($name == $confirmation) {
          $_SESSION['user'] = $name;
          return '
            <li class="nav-item mx-0 mx-lg-1">
              <h4 class="text-uppercase mb-4" Style="color: white; height: 90%; display: flex; align-items: center;">'.base64_decode($_SESSION['user']).'  </h4>
            </li>
            <li class="nav-item mx-0 mx-lg-1">
              <form action="" method="post" Style="height: 90%; display: flex; align-items: center;">
                <input type="hidden" name="logout" type="text" >
                <input class="btn btn-primary" type="submit" value="Disconnect">
              </form>
            </li>';
        } else {
          return $iniciasecio."</br><div class='alert alert-secondary' id='alert' role='alert'>Error!</div>";
        }
      }else {
        return $iniciasecio."</br><div class='alert alert-secondary' id='alert' role='alert'>Wrong name or password!</div>";
      }
    }else{
      return $iniciasecio;
    }
  }
} ?>