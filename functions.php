<?php
//Welcome to funtions.php on this document we will prepare all the information to show on profile.php

/* --------------------------------------------------------------------------------------------
   --------------------------------------------------------------------------------------------
We have a two first functions that will get our name/title when we want to put a link-reference on a profile (ex. the profile of Yoda will have as a planet: Naboo, on this function, we convert the url provide from the api to a name)
*/
function getName($id){
	$apiReponse = file_get_contents($id);
	// Convert JSON string to Array
	$apiArray = json_decode($apiReponse, true);
	return $apiArray['name'];
}
function getTitle($id){
	$apiReponse = file_get_contents($id);
	// Convert JSON string to Array
	$apiArray = json_decode($apiReponse, true);
	return $apiArray['title'];
}

/* --------------------------------------------------------------------------------------------
   --------------------------------------------------------------------------------------------
The following 5 functions will create a table-list to show all the references need. 
In that way we need only 5 functions and we can recreate everywhere
*/
function tablePeople($array){
	$tPReturn = '
		<div Class="everFlowIt">
    		<table class="table table-striped tableM" id="dest">
      			<thead class="thead-dark centus">
		        	<tr>
		        		<th scope="col">Characters</th>
		        	</tr>
		      	</thead>
		      	<tbody>';
	if (isset($array["characters"])) {
		foreach($array["characters"] as $mydata){
			$tPReturn .='<tr><td><a href="profile.php?id='.$mydata.'">'.getName($mydata).'</a></td></tr>';
		}
	}elseif (isset($array["pilots"])) {
		foreach($array["pilots"] as $mydata){
			$tPReturn .='<tr><td><a href="profile.php?id='.$mydata.'">'.getName($mydata).'</a></td></tr>';
		}
	}elseif (isset($array["residents"])) {
		foreach($array["residents"] as $mydata){
			$tPReturn .='<tr><td><a href="profile.php?id='.$mydata.'">'.getName($mydata).'</a></td></tr>';
		}
	}elseif (isset($array["people"])) {
		foreach($array["people"] as $mydata){
			$tPReturn .='<tr><td><a href="profile.php?id='.$mydata.'">'.getName($mydata).'</a></td></tr>';
		}
	}
  	$tPReturn .='
			    </tbody>
			</table>
		</div>';
	return $tPReturn;
}
function tablePlanets($array){
	$tPReturn = '
		<div Class="everFlowIt">
    		<table class="table table-striped tableM" id="dest">
      			<thead class="thead-dark centus">
		        	<tr>
		        		<th scope="col">Planets</th>
		        	</tr>
		      	</thead>
		      	<tbody>';
	if (isset($array["planets"])) {
		foreach($array["planets"] as $mydata){
			$tPReturn .='<tr><td><a href="profile.php?id='.$mydata.'">'.getName($mydata).'</a></td></tr>';
		}
	}elseif (isset($array["homeworld"])) {
		$tPReturn .='<tr><td><a href="profile.php?id='.$array["homeworld"].'">'.getName($array["homeworld"]).'</a></td></tr>';
	}
  	$tPReturn .='
			    </tbody>
			</table>
		</div>';
	return $tPReturn;
}
function tableSpecies($array){
	$tPReturn = '
		<div Class="everFlowIt">
    		<table class="table table-striped tableM" id="dest">
      			<thead class="thead-dark centus">
		        	<tr>
		        		<th scope="col">Species</th>
		        	</tr>
		      	</thead>
		      	<tbody>';
    foreach($array["species"] as $mydata){
		$tPReturn .='<tr><td><a href="profile.php?id='.$mydata.'">'.getName($mydata).'</a></td></tr>';
	}
  	$tPReturn .='
			    </tbody>
			</table>
		</div>';
	return $tPReturn;
}
function tableStarships($array){
	$tPReturn = '
		<div Class="everFlowIt">
    		<table class="table table-striped tableM" id="dest">
      			<thead class="thead-dark centus">
		        	<tr>
		        		<th scope="col">Starships</th>
		        	</tr>
		      	</thead>
		      	<tbody>';
    foreach($array["starships"] as $mydata){
		$tPReturn .='<tr><td><a href="profile.php?id='.$mydata.'">'.getName($mydata).'</a></td></tr>';
	}
  	$tPReturn .='
			    </tbody>
			</table>
		</div>';
	return $tPReturn;
}
function tableVehicles($array){
	$tPReturn = '
		<div Class="everFlowIt">
    		<table class="table table-striped tableM" id="dest">
      			<thead class="thead-dark centus">
		        	<tr>
		        		<th scope="col">Vehicles</th>
		        	</tr>
		      	</thead>
		      	<tbody>';
    foreach($array["vehicles"] as $mydata){
		$tPReturn .='<tr><td><a href="profile.php?id='.$mydata.'">'.getName($mydata).'</a></td></tr>';
	}
  	$tPReturn .='
			    </tbody>
			</table>
		</div>';
	return $tPReturn;
}
function tableFilms($array){
	$tPReturn = '
		<div Class="everFlowIt">
    		<table class="table table-striped tableM" id="dest">
      			<thead class="thead-dark centus">
		        	<tr>
		        		<th scope="col">Films</th>
		        	</tr>
		      	</thead>
		      	<tbody>';
    foreach($array["films"] as $mydata){
		$tPReturn .='<tr><td><a href="profile.php?id='.$mydata.'">'.getTitle($mydata).'</a></td></tr>';
	}
  	$tPReturn .='
			    </tbody>
			</table>
		</div>';
	return $tPReturn;
}

/* --------------------------------------------------------------------------------------------
   --------------------------------------------------------------------------------------------
The following 5 functions will create the main content.
*/
function film($apiArray){
	$end = '<div class="row">
				<div class="col-md-12">
					<h1 class="profileh1">'.$apiArray["title"].' '.$apiArray['episode_id'].'</h1>
				</div>
            </div>
			<div class="row">
				<div class="col-md-12"><h3 align="justify">'.$apiArray["opening_crawl"].'</h3></div>
            </div>
			<div class="row">
                <div class="col-md-4"><p><b>Director:</b> '.$apiArray["director"].'</p></div>
                <div class="col-md-4"><p><b>Producer:</b> '.$apiArray["producer"].'</p></div>
                <div class="col-md-4"><p><b>Release date:</b> '.$apiArray["release_date"].'</p></div>
            </div>
			<div class="row">
				<div class="col-md-4">'.
			      	tablePeople($apiArray).'
			    </div>
				<div class="col-md-4">'.
					tablePlanets($apiArray).
					tableSpecies($apiArray).'
				</div>
				<div class="col-md-4">'.
					tableStarships($apiArray).
					tableVehicles($apiArray).'
				</div>
            </div>	';
            return $end;
}
function people($apiArray){
	$end = '<div class="row">
				<div class="col-md-12">
					<h1 class="profileh1">'.$apiArray["name"].' ('.$apiArray['birth_year'].')</h1>
				</div>
            </div>
			<div class="row">
                <div class="col-md-4"><p><b>Gender:</b> '.$apiArray["gender"].'</p></div>
                <div class="col-md-4"><p><b>Height:</b> '.$apiArray["height"].'</p></div>
                <div class="col-md-4"><p><b>Mass:</b> '.$apiArray["mass"].'</p></div>
            </div>
			<div class="row">
                <div class="col-md-4"><p><b>Eye color:</b> '.$apiArray["eye_color"].'</p></div>
                <div class="col-md-4"><p><b>Hair color:</b> '.$apiArray["hair_color"].'</p></div>
                <div class="col-md-4"><p><b>Skin color:</b> '.$apiArray["skin_color"].'</p></div>
            </div>
			<div class="row">
				<div class="col-md-4">'.
			      	tableFilms($apiArray).'
			    </div>
				<div class="col-md-4">'.
					tablePlanets($apiArray).
					tableSpecies($apiArray).'
				</div>
				<div class="col-md-4">'.
					tableStarships($apiArray).
					tableVehicles($apiArray).'
				</div>
            </div>	';
            return $end;
}
function starships($apiArray){
	$end = '<div class="row">
				<div class="col-md-12">
					<h1 class="profileh1">'.$apiArray["name"].' ('.$apiArray['model'].')</h1>
				</div>
            </div>
			<div class="row">
                <div class="col-md-3"><p><b>Starship class:</b> '.$apiArray["starship_class"].'</p></div>
                <div class="col-md-3"><p><b>Manufacturer:</b> '.$apiArray["manufacturer"].'</p></div>
                <div class="col-md-3"><p><b>Cost in credits:</b> '.$apiArray["cost_in_credits"].'</p></div>
                <div class="col-md-3"><p><b>Length:</b> '.$apiArray["length"].'</p></div>
            </div>
			<div class="row">
                <div class="col-md-3"><p><b>Crew:</b> '.$apiArray["crew"].'</p></div>
                <div class="col-md-3"><p><b>Passengers:</b> '.$apiArray["passengers"].'</p></div>
                <div class="col-md-3"><p><b>Max atmosphering speed:</b> '.$apiArray["max_atmosphering_speed"].'</p></div>
                <div class="col-md-3"><p>Hyperdrive rating:</b> '.$apiArray["hyperdrive_rating"].'</p></div>
            </div>
			<div class="row">
                <div class="col-md-4"><p><b>MGLT:</b> '.$apiArray["MGLT"].'</p></div>
                <div class="col-md-4"><p><b>Cargo capacity:</b> '.$apiArray["cargo_capacity"].'</p></div>
                <div class="col-md-4"><p><b>Consumables:</b> '.$apiArray["consumables"].'</p></div>
            </div>
			<div class="row">
				<div class="col-md-6">'.
			      	tableFilms($apiArray).'
			    </div>
				<div class="col-md-6">'.
					tablePeople($apiArray).'
				</div>
            </div>	';
            return $end;
}
function vehicles($apiArray){
	$end = '<div class="row">
				<div class="col-md-12">
					<h1 class="profileh1">'.$apiArray["name"].' ('.$apiArray['model'].')</h1>
				</div>
            </div>
			<div class="row">
                <div class="col-md-4"><p><b>Vehicle class:</b> '.$apiArray["vehicle_class"].'</p></div>
                <div class="col-md-4"><p><b>Manufacturer:</b> '.$apiArray["manufacturer"].'</p></div>
                <div class="col-md-4"><p><b>Cost in credits:</b> '.$apiArray["cost_in_credits"].'</p></div>
            </div>
			<div class="row">
                <div class="col-md-4"><p><b>Length:</b> '.$apiArray["length"].'</p></div>
                <div class="col-md-4"><p><b>Crew:</b> '.$apiArray["crew"].'</p></div>
                <div class="col-md-4"><p><b>Passengers:</b> '.$apiArray["passengers"].'</p></div>
            </div>
			<div class="row">
                <div class="col-md-4"><p><b>Max atmosphering speed:</b> '.$apiArray["max_atmosphering_speed"].'</p></div>
                <div class="col-md-4"><p><b>Cargo capacity:</b> '.$apiArray["cargo_capacity"].'</p></div>
                <div class="col-md-4"><p><b>Consumables:</b> '.$apiArray["consumables"].'</p></div>
            </div>
			<div class="row">
				<div class="col-md-6">'.
			      	tableFilms($apiArray).'
			    </div>
				<div class="col-md-6">'.
					tablePeople($apiArray).'
				</div>
            </div>	';
            return $end;
}
function species($apiArray){
	$end = '<div class="row">
				<div class="col-md-12">
					<h1 class="profileh1">'.$apiArray["name"].'</h1>
				</div>
            </div>
			<div class="row">
                <div class="col-md-3"><p><b>Classification:</b> '.$apiArray["classification"].'</p></div>
                <div class="col-md-3"><p><b>Designation:</b> '.$apiArray["designation"].'</p></div>
                <div class="col-md-3"><p><b>Average height:</b> '.$apiArray["average_height"].'</p></div>
                <div class="col-md-3"><p><b>Average lifespan:</b> '.$apiArray["average_lifespan"].'</p></div>
            </div>
			<div class="row">
                <div class="col-md-3"><p><b>Eye colors:</b> '.$apiArray["eye_colors"].'</p></div>
                <div class="col-md-3"><p><b>Hair colors:</b> '.$apiArray["hair_colors"].'</p></div>
                <div class="col-md-3"><p><b>Skin colors:</b> '.$apiArray["skin_colors"].'</p></div>
                <div class="col-md-3"><p><b>Language:</b> '.$apiArray["language"].'</p></div>
            </div>
			<div class="row">
				<div class="col-md-4">'.
			      	tableFilms($apiArray).'
			    </div>
				<div class="col-md-4">'.
					tablePeople($apiArray).'
				</div>
				<div class="col-md-4">'.
					tablePlanets($apiArray).'
				</div>
            </div>	';
            return $end;
}
function planets($apiArray){
	$end = '<div class="row">
				<div class="col-md-12">
					<h1 class="profileh1">'.$apiArray["name"].'</h1>
				</div>
            </div>
			<div class="row">
                <div class="col-md-3"><p><b>Diameter:</b> '.$apiArray["diameter"].'</p></div>
                <div class="col-md-3"><p><b>Rotation period:</b> '.$apiArray["rotation_period"].'</p></div>
                <div class="col-md-3"><p><b>Orbital period:</b> '.$apiArray["orbital_period"].'</p></div>
                <div class="col-md-3"><p><b>Gravity:</b> '.$apiArray["gravity"].'</p></div>
            </div>
			<div class="row">
                <div class="col-md-3"><p><b>Population:</b> '.$apiArray["population"].'</p></div>
                <div class="col-md-3"><p><b>Climate:</b> '.$apiArray["climate"].'</p></div>
                <div class="col-md-3"><p><b>Terrain:</b> '.$apiArray["terrain"].'</p></div>
                <div class="col-md-3"><p><b>Surface water:</b> '.$apiArray["surface_water"].'</p></div>
            </div>
			<div class="row">
				<div class="col-md-6">'.
			      	tableFilms($apiArray).'
			    </div>
				<div class="col-md-6">'.
					tablePeople($apiArray).'
				</div>
            </div>	';
            return $end;
}
?>