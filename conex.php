<?php
function openDb(){
    $db_host = 'localhost';//Your host
    $db_user = 'root';//Username to conect to DB
    $db_pass = '';//Password to conect to DB
    $db_database = 'kamino';//DB

    $db = mysqli_connect($db_host, $db_user, $db_pass, $db_database);
    if(!$db) {
        die(mysqli_connect_error());
    }
    mysqli_query($db, "SET NAMES 'utf8'");
    return $db;
}
function executa_query($query, $link){
    $resultat = mysqli_query($link, $query);
    if(!$resultat){
        return mysqli_connect_error();
    }else{
        return $resultat;
    }
}
function tancarBD($link){
    mysqli_close($link);
} ?>