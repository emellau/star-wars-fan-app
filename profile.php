<?php include 'header.php'; include 'functions.php'; ?>
    <section class="masthead text-center mainDiv" id="portfolio">
      	<div class="container containerB">
	      	<?php
	      		if (!isset($_GET['id'])) { exit();}//Avoid non id error
	      		// We avoid other information, on the future, it would be grat to check possible SQL injection Etz...
	      		if (substr($_GET['id'], 0, 21) != "https://swapi.co/api/") { exit();}
				$apiReponse = file_get_contents($_GET['id']);
				//$apiReponse = file_get_contents("https://swapi.co/api/films/1/");
	      		if (!$apiReponse) { exit();}
				// Convert JSON string to Array
				$apiArray = json_decode($apiReponse, true);
				// Depending on what comes as id we call the corresponding function
				if (substr($_GET['id'], 21, 3) == "fil") {
					$content = film($apiArray);
				}elseif (substr($_GET['id'], 21, 3) == "peo") {
					$content = people($apiArray);
				}elseif (substr($_GET['id'], 21, 3) == "pla") {
					$content = planets($apiArray);
				}elseif (substr($_GET['id'], 21, 3) == "spe") {
					$content = species($apiArray);
				}elseif (substr($_GET['id'], 21, 3) == "sta") {
					$content = starships($apiArray);
				}elseif (substr($_GET['id'], 21, 3) == "veh") {
					$content = vehicles($apiArray);
				}else{
					$content = "Error";
				}
				echo($content);
			?>			
		</div>
    </section>
<?php $sort = 0; include 'footer.php'; ?>