function list(){//When call, query show.php to get the new list results by select, on success change the list and the title
  $(function() {
    var value = document.getElementById("listSelect").value;
    jQuery.ajax({
      url: 'show.php',
      method: 'post',
      data: "value="+value,
      async: true,
      beforeSend: function (){
        document.getElementById("dest").innerHTML = "<div class='loader'><div class='loader-child'></div></div>";
      },
      success: function(data){
        document.getElementById("dest").innerHTML = data;
        document.getElementById("title").innerHTML = value.charAt(0).toUpperCase() + value.slice(1);
      }
    });
  });
}
function searchIt(){//When call, query show.php to get the new list results by reseach, on success change the list and the title
  $(function() {
    var value = document.getElementById("textSeach").value;
    jQuery.ajax({
      url: 'show.php',
      method: 'post',
      data: "search="+value,
      async: true,
      beforeSend: function (){
        document.getElementById("dest").innerHTML = "<div class='loader'><div class='loader-child'></div></div>";
      },
      success: function(data){
        document.getElementById("dest").innerHTML = data;
      }
    });
  });
}
setTimeout(function() {
  $("#alert").fadeOut();
}, 3500);