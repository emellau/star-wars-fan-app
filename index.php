<?php include 'header.php'; ?>
    <section class="masthead text-center mainDiv" id="portfolio">
      <div class="container containerB">
			<h1 id="title" class="mainh1"><?php if(isset($_COOKIE["search"])){echo(ucfirst($_COOKIE["search"]));}else{echo ucfirst("films");}  ?></h1>
    		<!-- For the future, set a DB to get options and multilingual -->
			<select class="custom-select my-1 mr-sm-2 listSelect" id="listSelect" onChange="list()">
			    <option selected>Choose...</option>
			    <option value="films">Films</option>
			    <option value="people">People</option>
			    <option value="planets">Planets</option>
			    <option value="species">Species</option>
			    <option value="starships">Starships</option>
			    <option value="vehicles">Vehicles</option>
			</select>
			<button class="btn btn-info floating" onclick="searchIt()">Search</button>
			<input type="text" class="form-control mb-2 mr-sm-2 floating" id="textSeach" placeholder="Search.."></br></br>
			<div id="dest" class="dest">
				<?php include 'show.php'; ?>
			</div>
		</div>
    </section>
<?php include 'footer.php'; ?>