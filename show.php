<?php
if(isset($_COOKIE["search"])){$search=($_COOKIE["search"]);}else{$search="films";}//Get cookie if posible
if(isset($_POST['value'])) {$search=$_POST['value']; setcookie("search",$_POST['value']);}//Get value from jQuery if posible
if (isset($_POST['search'])) {//Get search from jQuery if posible
  $apiReponse = file_get_contents("https://swapi.co/api/".$search."/?search=".$_POST['search']);
}else{
  $apiReponse = file_get_contents("https://swapi.co/api/".$search);
}
if (!$apiReponse) { exit();}//On file_get_contents error exit
// Convert JSON string to Array
$apiArray = json_decode($apiReponse, true);
// Array to print table, if where i bigger project i would prefere to use a DB to alse set multilengual
$bTable = array (
    "films"         => array("Title", "Episode", "Director", "Producer"),
    "films_id"      => array("title", "episode_id", "director", "producer"),
    "people"        => array("Name", "Gender", "Birth year", "Hair color"),
    "people_id"     => array("name", "gender", "birth_year", "hair_color"),
    "planets"       => array("Name", "Population", "Surface water", "Gravity"),
    "planets_id"    => array("name", "population", "surface_water", "gravity"),
    "species"       => array("Name", "Classification", "Language", "Average lifespan"),
    "species_id"    => array("name", "classification", "language", "average_lifespan"),
    "starships"     => array("Name", "Model", "Crew", "Cost in credits"),
    "starships_id"  => array("name", "model", "crew", "cost_in_credits"),
    "vehicles"      => array("Name", "Model", "Crew", "Cost in credits"),
    "vehicles_id"   => array("name", "model", "crew", "cost_in_credits")
);
// Set table
$table='
      <div style="overflow-x:auto;">
        <table class="table table-striped tableM" id="dest">
          <thead class="thead-dark" style="text-align: center;">
            <tr>';
// Print table titles
foreach($bTable[$search] as $mydata){            
  $table .=  '<th scope="col">'.$mydata.'</th>';
}
$table .=  '</tr>
          </thead>
          <tbody>
            <tr>';
// Print table rows
foreach($apiArray["results"] as $mydata){
  $table .='<tr>';
  $count = 0;
  foreach($bTable[$search."_id"] as $id){  
    if ($count == 0) {
      $count++;
      $table .="<td><a href='profile.php?id=".$mydata["url"]."'>".$mydata[$id].'</a></td>';
    }else{
      $table .='<td>'.$mydata[$id].'</td>';
    }        
  }
  $table .="</tr>";
}
// finish table
$table .=  '
          </tbody>
        </table>
      </div>';
echo($table);
?>