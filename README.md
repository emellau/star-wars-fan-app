## SYNOPSIS
  
![logo](https://img.20mn.fr/9c1s2hwvSx2OJTe_gC8QfQ/310x190_ewok-wicket-retour-jedi.jpg)

__star-wars-fan-app__ is a web app where a user can get a quick overview over all Star Wars
resources like heroes, planets.... 

  
## INSTALLATION

Clone the repository and follow the steps: 


* Put all the documents on your web server
* Create a DB
* Add user.sql to your DB (It will create a table on it)
* Changhe conex.php to point your DB


## USAGE

The code is documented

## License

PhpSpreadsheet is licensed under [LGPL (GNU LESSER GENERAL PUBLIC LICENSE)](https://github.com/PHPOffice/PhpSpreadsheet/blob/master/LICENSE)