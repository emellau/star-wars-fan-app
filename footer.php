      <footer class="mastfoot mt-auto">
  	    <div class="copyright py-4 text-center text-white">
  	      <div class="container">
  	        <medium>EMellau &copy; 2018</medium>
  	      </div>
  	    </div>
      </footer>
    <!-- Bootstrap core JavaScript -->
    <script src="vendor/jquery/jquery.min.js"></script>

    <!-- Custom scripts for the challenge -->
    <script src="js/ajaxs.js"></script>

    <?php if (@$sort != 0) { ?>
    <!--***** Table sort *****--> 
    <script src="js/sorttable.js"></script>
    <script type="text/javascript">
        $("table").addSortWidget();
    </script>

    <?php } ?>
  </body>
</html>
<?php tancarBD($link); ?>